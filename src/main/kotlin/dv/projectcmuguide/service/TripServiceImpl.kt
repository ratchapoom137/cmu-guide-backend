package dv.projectcmuguide.service

import dv.projectcmuguide.dao.CustomerDao
import dv.projectcmuguide.dao.TripDao
import dv.projectcmuguide.dao.TripOrderDao
import dv.projectcmuguide.entity.Trip
import dv.projectcmuguide.entity.TripOrder
import dv.projectcmuguide.entity.dto.TripDto
import dv.projectcmuguide.entity.dto.TripId
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service
class TripServiceImpl : TripService {
    @Transactional
    override fun deleteTripByTripId(tripId: Long): Trip {
        val trip = tripDao.findTripById(tripId)
        trip?.isDeleted = true
        var tripOrder = tripOrderDao.findTripOrderByTripId(tripId)
        for (item in tripOrder){
            item?.isDeleted = true
        }
        return trip
    }
//    override fun addTripOrder(tripOrder: TripOrder, tripId: Long): Trip {
//        val tripMap = tripDao.getTripByTripId(tripId)
//        tripMap.tripOrder.add(tripOrder)
//        return tripDao.save(tripMap)
//    }

    override fun getTripByUserId(userId: Long): List<TripDto> {
        return tripDao.getTripByUserId(userId)
    }

    override fun save(mapTrip: Trip): Trip {
        return tripDao.save(mapTrip)
    }

    override fun addTripByUserId(trip: Trip): Trip {
        var customerMap = customerDao.findById(trip.customer!!.id!!)
        var addTrip = Trip()
        addTrip.name = trip.name
        addTrip.description = trip.description
        addTrip.customer = customerMap
        return tripDao.save(addTrip)
    }

    @Autowired
    lateinit var tripDao: TripDao

    @Autowired
    lateinit var tripOrderDao: TripOrderDao

    @Autowired
    lateinit var customerDao: CustomerDao
}