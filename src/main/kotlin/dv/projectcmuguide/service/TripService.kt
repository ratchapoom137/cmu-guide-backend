package dv.projectcmuguide.service

import dv.projectcmuguide.entity.Trip
import dv.projectcmuguide.entity.TripOrder
import dv.projectcmuguide.entity.dto.TripDto
import dv.projectcmuguide.entity.dto.TripId

interface TripService {
    fun addTripByUserId(trip: Trip): Trip
    fun save(mapTrip: Trip): Trip
    fun getTripByUserId(userId: Long): List<TripDto>
    fun deleteTripByTripId(tripId: Long): Trip
//    fun addTripOrder(tripOrder: TripOrder, tripId: Long): Trip
}