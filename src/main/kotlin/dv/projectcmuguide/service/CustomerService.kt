package dv.projectcmuguide.service

import dv.projectcmuguide.entity.Customer
import dv.projectcmuguide.entity.dto.CustomerDto

interface CustomerService {
    fun getUserById(id: Long): Customer
    fun save(customer: Customer): Customer
    fun editUser(customer: CustomerDto, id: Long): Customer
    fun saveImage(id: Long, imageUrl: String): Customer
    fun findByEmail(email: String?): Customer?
}