package dv.projectcmuguide.service

import dv.projectcmuguide.dao.TripDao
import dv.projectcmuguide.dao.TripOrderDao
import dv.projectcmuguide.entity.TripOrder
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service
class TripOrderServiceImpl : TripOrderService {
    @Transactional
    override fun deleteTripOrderByTripOrderId(tripOrderId: Long): TripOrder {
        val tripOrder = tripOrderDao.findTripOrderById(tripOrderId)
        tripOrder?.isDeleted = true
        return tripOrder
    }

    override fun addTripOrderByTripId(tripId: Long, tripOrder: TripOrder): TripOrder {
        val trip = tripDao.findTripById(tripId)
        val addTrip = TripOrder()
        addTrip.oreder = tripOrder.oreder
        addTrip.shopId = tripOrder.shopId
        addTrip.trip = trip
        return tripOrderDao.save(addTrip)
    }

    override fun getTripOrderByTripId(tripId: Long): List<TripOrder> {
        return tripOrderDao.findTripOrderByTripId(tripId)
    }

    @Autowired
    lateinit var tripDao: TripDao

    @Autowired
    lateinit var tripOrderDao: TripOrderDao
}