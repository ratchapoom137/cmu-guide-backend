package dv.projectcmuguide.service

import dv.projectcmuguide.dao.ReviewDao
import dv.projectcmuguide.entity.Review
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class ReviewServiceImpl: ReviewService {
    override fun editReview(review: Review): Review {
        var reviewMap = reviewDao.findById(review.id)
        reviewMap.comment = review.comment
        reviewMap.rate = review.rate
        reviewMap.date = review.date
        return reviewDao.save(reviewMap)
    }

    override fun deleteReview(id: Long): Review {
        var review = reviewDao.findById(id)
        review?.isDeleted = true
        return reviewDao.save(review)
    }

    override fun save(review: Review): Review {
        return reviewDao.save(review)
    }

    override fun getReviewByShopId(id: String): List<Review> {
        return  reviewDao.getReviewByShopId(id)
    }

    override fun getReviewByUserId(id: Long): List<Review> {
        return  reviewDao.getReviewByUserId(id)
    }

    @Autowired
    lateinit var reviewDao: ReviewDao
}