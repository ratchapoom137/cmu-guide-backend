package dv.projectcmuguide.service

import dv.projectcmuguide.entity.TripOrder

interface TripOrderService {
    fun getTripOrderByTripId(tripId: Long): List<TripOrder>
    fun addTripOrderByTripId(tripId: Long, tripOrder: TripOrder): TripOrder
    fun deleteTripOrderByTripOrderId(tripOrderId: Long): TripOrder

}