package dv.projectcmuguide.service

import dv.projectcmuguide.entity.Review

interface ReviewService {
    fun getReviewByUserId(id: Long): List<Review>
    fun getReviewByShopId(id: String): List<Review>
    fun save(review: Review): Review
    fun editReview(review: Review): Review
    fun deleteReview(id: Long): Review
}
