package dv.projectcmuguide.service

import dv.projectcmuguide.dao.CustomerDao
import dv.projectcmuguide.entity.Customer
import dv.projectcmuguide.entity.dto.CustomerDto
import dv.projectcmuguide.repository.CustomerRepository
import dv.projectcmuguide.security.entity.AuthorityName
import dv.projectcmuguide.security.entity.JwtUser
import dv.projectcmuguide.security.repository.AuthorityRepository
import dv.projectcmuguide.security.repository.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Service

@Service
class CustomerServiceImpl : CustomerService {
    override fun findByEmail(email: String?): Customer? {
        val customer =  customerDao.findByEmail(email)
        return customer
    }


    override fun saveImage(id: Long, imageUrl: String): Customer {
        val customer = customerDao.findById(id)
        customer.image = imageUrl
        return customerDao.save(customer)
    }

    override fun editUser(customer: CustomerDto, id: Long): Customer {
        val customerMap = customerDao.findById(id)
        customerMap.firstName = customer.firstName
        customerMap.lastName = customer.lastName
        return customerDao.save(customerMap)
    }

    override fun save(customer: Customer): Customer {
       return customerDao.save(customer)
    }

    override fun getUserById(id: Long): Customer {
        return customerDao.getUserById(id)
    }

    @Autowired
    lateinit var userRepository: UserRepository

    @Autowired
    lateinit var authorityRepository: AuthorityRepository

    @Autowired
    lateinit var customerRepository: CustomerRepository

    @Autowired
    lateinit var customerDao: CustomerDao
}