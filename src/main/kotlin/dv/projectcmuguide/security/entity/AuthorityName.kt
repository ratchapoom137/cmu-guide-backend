package dv.projectcmuguide.security.entity

enum class AuthorityName{
    ROLE_CUSTOMER, ROLE_ADMIN, ROLE_GENERAL
}