package dv.projectcmuguide.security.repository

import dv.projectcmuguide.security.entity.JwtUser
import org.springframework.data.repository.CrudRepository

interface UserRepository: CrudRepository<JwtUser, Long> {
    fun findByUsername(username: String): JwtUser
}