package dv.projectcmuguide.security.repository

import dv.projectcmuguide.security.entity.Authority
import dv.projectcmuguide.security.entity.AuthorityName
import org.springframework.data.repository.CrudRepository

interface AuthorityRepository: CrudRepository<Authority, Long> {
    fun findByName(input: AuthorityName): Authority
}