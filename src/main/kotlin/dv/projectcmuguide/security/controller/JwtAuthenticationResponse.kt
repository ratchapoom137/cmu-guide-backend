package dv.projectcmuguide.security.controller

data class JwtAuthenticationResponse(
        var token: String? = null
)