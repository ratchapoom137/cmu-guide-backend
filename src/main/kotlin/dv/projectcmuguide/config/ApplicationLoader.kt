package dv.projectcmuguide.config


import dv.projectcmuguide.entity.Customer
import dv.projectcmuguide.entity.Review
import dv.projectcmuguide.repository.CustomerRepository
import dv.projectcmuguide.repository.ReviewRepository
import dv.projectcmuguide.security.entity.Authority
import dv.projectcmuguide.security.entity.AuthorityName
import dv.projectcmuguide.security.entity.JwtUser
import dv.projectcmuguide.security.repository.AuthorityRepository
import dv.projectcmuguide.security.repository.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Component
import javax.transaction.Transactional

@Component
class ApplicationLoader : ApplicationRunner {
    @Autowired
    lateinit var customerRepository: CustomerRepository
    @Autowired
    lateinit var reviewRepository: ReviewRepository
    @Autowired
    lateinit var authorityRepository: AuthorityRepository
    @Autowired
    lateinit var userRepository: UserRepository

    fun loadUsernameAndPassword() {
        val auth1 = Authority(name = AuthorityName.ROLE_ADMIN)
        val auth2 = Authority(name = AuthorityName.ROLE_CUSTOMER)
        val auth3 = Authority(name = AuthorityName.ROLE_GENERAL)
        authorityRepository.save(auth1)
        authorityRepository.save(auth2)
        authorityRepository.save(auth3)
        val encoder = BCryptPasswordEncoder()
        val custl = Customer(email = "testpoom@gmail.com", password = "password", firstName = "testpoooom", lastName = "eie", image = "https://www.img.in.th/images/b145632864b29a988732b0c73bc0d333.jpg")
        val custJwt = JwtUser(
                username = custl.email,
                password = encoder.encode(custl.password),
                email = custl.email,
                enabled = true,
                firstname = custl.firstName,
                lastname = custl.lastName
        )

        custl.jwtUser = custJwt
        custJwt.user = custl
        customerRepository.save(custl)
        userRepository.save(custJwt)
    }

    @Transactional
    override fun run(args: ApplicationArguments?) {
        customerRepository.save(Customer("poom@gmail.com", "1234", "rat", "cha", null))
        reviewRepository.save(Review("eiei", 5,"123456789", false,1,1555848668172))
        reviewRepository.save(Review("2222", 5,"123456789", false,1, 1555848762540))
        loadUsernameAndPassword()
    }
}