package dv.projectcmuguide.dao

import dv.projectcmuguide.entity.Customer

interface CustomerDao {
    fun getUserById(id: Long): Customer
    fun save(customer: Customer): Customer
    fun findById(id: Long): Customer
    fun findByEmail(email: String?): Customer?

}