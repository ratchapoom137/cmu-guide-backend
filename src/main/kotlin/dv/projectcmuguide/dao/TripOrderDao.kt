package dv.projectcmuguide.dao

import dv.projectcmuguide.entity.TripOrder

interface TripOrderDao {
    fun findTripOrderByTripId(tripId: Long): List<TripOrder>
    fun save(addTrip: TripOrder): TripOrder
    fun findTripOrderById(tripOrderId: Long): TripOrder

}