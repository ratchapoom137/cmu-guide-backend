package dv.projectcmuguide.dao

import dv.projectcmuguide.entity.TripOrder
import dv.projectcmuguide.repository.TripOrderRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class TripOrderDaoImpl: TripOrderDao {
    override fun findTripOrderById(tripOrderId: Long): TripOrder {
        return tripOrderRepository.findTripOrderById(tripOrderId)
    }

    override fun save(addTrip: TripOrder): TripOrder {
        return tripOrderRepository.save(addTrip)
    }

    override fun findTripOrderByTripId(tripId: Long): List<TripOrder> {
        return tripOrderRepository.findTripOrderByTripIdAndIsDeletedIsFalse(tripId)
    }

    @Autowired
    lateinit var tripOrderRepository: TripOrderRepository

}