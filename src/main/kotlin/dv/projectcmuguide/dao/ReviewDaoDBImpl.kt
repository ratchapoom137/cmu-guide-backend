package dv.projectcmuguide.dao

import dv.projectcmuguide.entity.Review
import dv.projectcmuguide.repository.ReviewRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class ReviewDaoDBImpl: ReviewDao {
    override fun findById(id: Long?): Review {
        return reviewRepository.findById(id!!).orElse(null)
    }

    override fun save(review: Review): Review {
        return reviewRepository.save(review)
    }

    override fun getReviewByShopId(id: String): List<Review> {
        return reviewRepository.findByShopId(id)
    }

    override fun getReviewByUserId(id: Long): List<Review> {
        return reviewRepository.findByCustomerId(id)
    }

    @Autowired
    lateinit var reviewRepository: ReviewRepository
}