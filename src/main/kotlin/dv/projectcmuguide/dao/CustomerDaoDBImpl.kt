package dv.projectcmuguide.dao

import dv.projectcmuguide.entity.Customer
import dv.projectcmuguide.entity.dto.CustomerDto
import dv.projectcmuguide.repository.CustomerRepository
import dv.projectcmuguide.security.entity.AuthorityName
import dv.projectcmuguide.security.entity.JwtUser
import dv.projectcmuguide.security.repository.AuthorityRepository
import dv.projectcmuguide.security.repository.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class CustomerDaoDBImpl: CustomerDao {
    override fun findByEmail(email: String?): Customer? {
        return customerRepository.findByEmail(email)
    }


    override fun findById(id: Long): Customer {
        return customerRepository.findById(id).orElse(null)
    }

    override fun save(customer: Customer): Customer {
        val authority = authorityRepository.findByName(AuthorityName.ROLE_CUSTOMER)
        val encoder = BCryptPasswordEncoder()
        val jwt = userRepository.save(JwtUser(customer.email, customer.firstName, customer.lastName, encoder.encode(customer.password), customer.email, true))
        val userSave = customerRepository.save(customer)
        jwt.user = userSave
        userSave.jwtUser = jwt
        jwt.authorities.add(authority)
        return customerRepository.save(customer)
    }

    override fun getUserById(id: Long): Customer {
        return customerRepository.findById(id).orElse(null)
    }

    @Autowired
    lateinit var userRepository: UserRepository

    @Autowired
    lateinit var authorityRepository: AuthorityRepository

    @Autowired
    lateinit var customerRepository: CustomerRepository
}