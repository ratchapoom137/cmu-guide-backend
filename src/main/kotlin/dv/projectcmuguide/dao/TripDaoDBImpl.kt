package dv.projectcmuguide.dao

import dv.projectcmuguide.entity.Trip
import dv.projectcmuguide.entity.dto.TripDto
import dv.projectcmuguide.entity.dto.TripId
import dv.projectcmuguide.repository.TripRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class TripDaoDBImpl: TripDao {
    override fun findTripById(tripId: Long): Trip {
        return tripRepository.findTripById(tripId)
    }

    override fun getTripByTripId(id: Long?): Trip {
        return tripRepository.findById(id!!).orElse(null)
    }

    override fun getTripByUserId(userId: Long): List<TripDto> {
        return tripRepository.findTripByCustomerIdAndIsDeletedIsFalse(userId)
    }

    override fun save(addTrip: Trip): Trip {
        return tripRepository.save(addTrip)
    }

    @Autowired
    lateinit var tripRepository: TripRepository

}