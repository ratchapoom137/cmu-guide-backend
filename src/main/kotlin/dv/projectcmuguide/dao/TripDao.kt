package dv.projectcmuguide.dao

import dv.projectcmuguide.entity.Trip
import dv.projectcmuguide.entity.TripOrder
import dv.projectcmuguide.entity.dto.TripDto
import dv.projectcmuguide.entity.dto.TripId

interface TripDao {
    fun save(addTrip: Trip): Trip
    fun getTripByUserId(userId: Long): List<TripDto>
    fun getTripByTripId(id: Long?): Trip
    fun findTripById(tripId: Long): Trip

}