package dv.projectcmuguide.dao

import dv.projectcmuguide.entity.Review

interface ReviewDao {
    fun getReviewByUserId(id: Long): List<Review>
    fun getReviewByShopId(id: String): List<Review>
    fun save(review: Review): Review
    fun findById(id: Long?): Review
}