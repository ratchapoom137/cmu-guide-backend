package dv.projectcmuguide

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class ProjectcmuguideApplication

fun main(args: Array<String>) {
    runApplication<ProjectcmuguideApplication>(*args)
}
