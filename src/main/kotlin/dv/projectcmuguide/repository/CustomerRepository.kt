package dv.projectcmuguide.repository

import dv.projectcmuguide.entity.Customer
import org.springframework.data.repository.CrudRepository

interface CustomerRepository: CrudRepository<Customer, Long> {
    fun findByEmail(email: String?): Customer?

}