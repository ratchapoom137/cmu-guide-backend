package dv.projectcmuguide.repository

import dv.projectcmuguide.entity.TripOrder
import dv.projectcmuguide.entity.dto.TripDto
import org.springframework.data.repository.CrudRepository

interface TripOrderRepository: CrudRepository<TripOrder, Long> {
    fun findTripOrderByTripIdAndIsDeletedIsFalse(tripId: Long): List<TripOrder>
    fun findTripOrderById(tripOrderId: Long): TripOrder
}