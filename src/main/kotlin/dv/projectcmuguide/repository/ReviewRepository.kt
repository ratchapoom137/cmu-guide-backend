package dv.projectcmuguide.repository


import dv.projectcmuguide.entity.Review
import org.springframework.data.repository.CrudRepository

interface ReviewRepository: CrudRepository<Review, Long> {
    fun findByShopId(id: String): List<Review>
    fun findByCustomerId(id: Long): List<Review>

}