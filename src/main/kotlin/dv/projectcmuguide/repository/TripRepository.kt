package dv.projectcmuguide.repository

import dv.projectcmuguide.entity.Trip
import dv.projectcmuguide.entity.dto.TripDto
import org.springframework.data.repository.CrudRepository

interface TripRepository: CrudRepository<Trip, Long> {
    fun findTripByCustomerIdAndIsDeletedIsFalse(userId: Long): List<TripDto>
    fun findTripById(tripId: Long): Trip
}