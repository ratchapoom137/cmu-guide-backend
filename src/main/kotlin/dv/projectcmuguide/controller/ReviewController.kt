package dv.projectcmuguide.controller

import dv.projectcmuguide.entity.Review
import dv.projectcmuguide.service.ReviewService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
class ReviewController {
    @Autowired
    lateinit var reviewService: ReviewService

    @GetMapping("/review/shop/{shopId}")
    fun getReviewByShopId(@PathVariable("shopId") id: String): ResponseEntity<Any> {
        val output = reviewService.getReviewByShopId(id)
        return ResponseEntity.ok(output)
    }

    @GetMapping("/review/user/{userId}")
    fun getReviewByUserId(@PathVariable("userId") id: Long): ResponseEntity<Any> {
        val output = reviewService.getReviewByUserId(id)
        return ResponseEntity.ok(output)
    }

    @PostMapping("/review/add")
    fun addReviewByUserId(@RequestBody review: Review): ResponseEntity<Any> {
        val output = reviewService.save(review)
        return ResponseEntity.ok(output)
    }

    @PutMapping("/review/edit")
    fun editReview(@RequestBody review: Review): ResponseEntity<Any> {
        val output = reviewService.editReview(review)
        return ResponseEntity.ok(output)
    }

    @DeleteMapping("/review/delete/{reviewId}")
    fun deleteReview(@PathVariable("reviewId") id: Long): ResponseEntity<Any> {
        val output = reviewService.deleteReview(id)
        return ResponseEntity.ok(output)
    }


}