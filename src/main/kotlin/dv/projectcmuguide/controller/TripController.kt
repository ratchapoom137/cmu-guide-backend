package dv.projectcmuguide.controller

import dv.projectcmuguide.entity.Trip
import dv.projectcmuguide.entity.TripOrder
import dv.projectcmuguide.entity.dto.TripId
import dv.projectcmuguide.repository.CustomerRepository
import dv.projectcmuguide.service.TripOrderService
import dv.projectcmuguide.service.TripService
import dv.projectcmuguide.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.transaction.Transactional

@RestController
class TripController {
    @Autowired
    lateinit var tripService: TripService

    @Autowired
    lateinit var tripOrderService: TripOrderService

    @Autowired
    lateinit var customerRepository: CustomerRepository

    @PostMapping("/trip/add")
    fun addTripByUserId(@RequestBody trip: Trip): ResponseEntity<Any> {
//        val output = tripService.addTripByUserId(trip)
//        val outputDto = MapperUtil.INSTANCE.mapTripDto(output)
//        outputDto?.let { return ResponseEntity.ok(it) }
//        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("not found")
//        return ResponseEntity.ok(output)
        val customerMap = MapperUtil.INSTANCE.mapCustomerTripDto(customerRepository.findById(trip.customer!!.id!!).orElse(null))
        val customerDto = MapperUtil.INSTANCE.mapCustomerTripDto(customerMap)
        trip.customer = customerDto
        val outputSave = tripService.save(trip)
//        var output = MapperUtil.INSTANCE.mapTripDto(trip)
        return ResponseEntity.ok(outputSave)
    }

    @GetMapping("/trip/list/{userId}")
    fun getTriByUserId(@PathVariable userId: Long): ResponseEntity<Any>{
        val output = tripService.getTripByUserId(userId)
//        val outputDto = MapperUtil.INSTANCE.mapTripDto(output)
        output?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("not found")
    }

    @GetMapping("/trip/listby/{tripId}")
    fun getTripByTrupId(@PathVariable tripId: Long): ResponseEntity<Any> {
        val output = tripOrderService.getTripOrderByTripId(tripId)
        val outputDto = MapperUtil.INSTANCE.mapTripOrderDto(output)
        var outputSortOrder = outputDto.sortedWith(compareBy({ it.oreder }))
        outputSortOrder?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("not found")
    }

    @PostMapping("/trip/add/triporder/{tripId}")
    fun addTripOrderByTripId(@PathVariable("tripId") tripId: Long,
                             @RequestBody tripOrder: TripOrder): ResponseEntity<Any> {
        val output = tripOrderService.addTripOrderByTripId(tripId, tripOrder)
        val outputDto = MapperUtil.INSTANCE.mapTripOrderDto(output)
        outputDto?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("not found")
    }

    @DeleteMapping("/trip/delete/{tripId}")
    fun deleteTripByTripId(@PathVariable tripId: Long): ResponseEntity<Any> {
        val output = tripService.deleteTripByTripId(tripId)
        val outputDto = MapperUtil.INSTANCE.mapTripDto(output)
        outputDto?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("not found")
    }

    @DeleteMapping("/trip/triporder/delete/{tripOrderId}")
    fun deleteTripOrderByTripOrderId(@PathVariable tripOrderId : Long): ResponseEntity<Any> {
        val output = tripOrderService.deleteTripOrderByTripOrderId(tripOrderId)
        val outputDto = MapperUtil.INSTANCE.mapTripOrderDto(output)
        outputDto?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("not found")
    }

}