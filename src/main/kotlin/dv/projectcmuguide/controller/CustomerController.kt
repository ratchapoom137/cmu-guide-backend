package dv.projectcmuguide.controller

import dv.projectcmuguide.entity.Customer
import dv.projectcmuguide.entity.dto.CustomerDto
import dv.projectcmuguide.repository.CustomerRepository
import dv.projectcmuguide.service.AmazonClient
import dv.projectcmuguide.service.CustomerService
import dv.projectcmuguide.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile

@RestController
class CustomerController {
    @Autowired
    lateinit var customerService: CustomerService

    @Autowired
    lateinit var amazonClient: AmazonClient

    @Autowired
    lateinit var customerRepository: CustomerRepository

    @GetMapping("/user/{userId}")
    fun getUserById(@PathVariable("userId") id: Long): ResponseEntity<Any> {
        val customer = customerService.getUserById(id)
        val customerDto = MapperUtil.INSTANCE.mapCustomerRegister(customer)
        return ResponseEntity.ok(customerDto)
    }

    @PutMapping("/user/edit/{userId}")
    fun editUser(@RequestBody customer: CustomerDto,
                 @PathVariable("userId") id: Long): ResponseEntity<Any> {
        val output = customerService.editUser(customer, id)
        val outputDto = MapperUtil.INSTANCE.mapCustomerEditDto(output)
        return ResponseEntity.ok(outputDto)
    }

    @PostMapping("/user/register")
    fun addUser(@RequestBody customer: Customer): ResponseEntity<Any> {
        val output = MapperUtil.INSTANCE.mapCustomerDto(customerService.findByEmail(customer.email))
        output?.let { return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).build() }
        val customer = customerService.save(customer)
        val customerDto = MapperUtil.INSTANCE.mapCustomerRegister(customer)
        return ResponseEntity.ok(customerDto)
    }

    @PostMapping("/user/uploadImage/{userId}")
    fun uploadImage(@RequestPart(value = "file") file: MultipartFile,
                    @PathVariable("userId") id: Long): ResponseEntity<*> {
        var imageUrl = this.amazonClient.uploadFile(file)
        var userId = customerService.saveImage(id,imageUrl)
        var output = MapperUtil.INSTANCE.mapCustomerUploadImageDto(userId)
        return ResponseEntity.ok(output)
    }
}