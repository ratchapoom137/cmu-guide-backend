package dv.projectcmuguide.util

import dv.projectcmuguide.entity.Customer
import dv.projectcmuguide.entity.Trip
import dv.projectcmuguide.entity.TripOrder
import dv.projectcmuguide.entity.dto.*
import org.mapstruct.Mapper
import org.mapstruct.factory.Mappers

@Mapper(componentModel = "spring")
interface MapperUtil {
    companion object {
        val INSTANCE = Mappers.getMapper(MapperUtil::class.java)
    }

    fun mapCustomerDto(customer: Customer?): CustomerDto?

    fun mapCustomerRegister(customer: Customer): CustomerDto

    fun mapCustomerEditDto(customer: Customer): CustomerEditDto

    fun mapCustomerUploadImageDto(customer: Customer): CustomerUploadImageDto

    fun mapUser(customer: Customer): UserDto

    fun mapTripDto(trip: Trip): TripDto
    fun mapTripDto(trip: List<Trip>): List<TripDto>

    fun mapCustomerTripDto(customer: CustomerDto): Customer
    fun mapCustomerTripDto(customer: Customer): CustomerDto

    fun mapTripOrderDto(tripData: TripOrder): TripOrderDto
    fun mapTripOrderDto(tripData: List<TripOrder>): List<TripOrderDto>
}