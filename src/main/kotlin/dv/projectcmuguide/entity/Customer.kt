package dv.projectcmuguide.entity

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
data class Customer(
        override var email: String? = null,
        override var password: String? = null,
        var firstName: String? = null,
        var lastName: String? = null,
        var image: String? = null
) : User(email, password) {

}