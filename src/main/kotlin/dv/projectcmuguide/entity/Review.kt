package dv.projectcmuguide.entity

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.ManyToOne

@Entity
data class Review(
        var comment: String? = null,
        var rate: Int? = null,
        var shopId: String? = null,
        var isDeleted: Boolean = false,
        var customerId: Long? = null,
        var date: Long? = null
) {
    @Id
    @GeneratedValue
    var id: Long? = null
}