package dv.projectcmuguide.entity

import javax.persistence.*

@Entity
data class Trip(
        var name: String? = null,
        var description: String? = null,
        var isDeleted: Boolean = false
) {
    @Id
    @GeneratedValue
    var id: Long? = null

    @ManyToOne
    var customer: Customer? = null

//    @OneToMany
//    var tripOrder = mutableListOf<TripOrder>()
}