package dv.projectcmuguide.entity.dto

import dv.projectcmuguide.entity.Customer
import dv.projectcmuguide.entity.TripOrder

data class TripDto(
        var id: Long? = null,
        var name: String? = null,
        var description: String? = null
)

data class TripId(
        var id: Long?
)