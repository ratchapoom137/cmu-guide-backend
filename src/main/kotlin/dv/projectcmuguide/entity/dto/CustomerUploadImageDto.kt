package dv.projectcmuguide.entity.dto

data class CustomerUploadImageDto(
        var image: String? = null
)