package dv.projectcmuguide.entity.dto

data class CustomerEditDto(
        var firstName: String? = null,
        var lastName: String? = null
)