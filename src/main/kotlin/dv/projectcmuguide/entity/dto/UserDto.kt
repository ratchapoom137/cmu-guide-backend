package dv.projectcmuguide.entity.dto

data class UserDto(
        var id: Long? = null,
        var email: String? = null,
        var firstName: String? = null,
        var lastName: String? = null,
        var image: String? = null
)