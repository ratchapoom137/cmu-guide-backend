package dv.projectcmuguide.entity.dto

import dv.projectcmuguide.entity.Trip

data class TripOrderDto(
        var id: Long? = null,
        var oreder: Int? = null,
        var shopId: String? = null,
        var trip : TripDto? = null
)