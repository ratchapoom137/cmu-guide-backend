package dv.projectcmuguide.entity.dto

data class CustomerDto(
        var id: Long? = null,
        var email: String? = null,
        var firstName: String? = null,
        var lastName: String? = null,
        var image: String? = null
)

data class CustomerDtoNew(
        var id: Long? = null,
        var password: String? = null,
        var email: String? = null,
        var firstName: String? = null,
        var lastName: String? = null,
        var image: String? = null
)

data class CustomerTripDto (
        var id: Long? = null,
        var password: String? = null,
        var email: String? = null,
        var firstName: String? = null,
        var lastName: String? = null,
        var image: String? = null
)