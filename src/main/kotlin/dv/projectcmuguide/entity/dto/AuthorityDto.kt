package dv.projectcmuguide.entity.dto

data class AuthorityDto(var name: String? = null)