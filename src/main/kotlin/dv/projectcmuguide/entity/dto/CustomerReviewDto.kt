package dv.projectcmuguide.entity.dto

data class CustomerReviewDto(
        var id: Long? = null
)