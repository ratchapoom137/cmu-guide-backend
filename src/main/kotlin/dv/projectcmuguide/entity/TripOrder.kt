package dv.projectcmuguide.entity

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.ManyToOne

@Entity
data class TripOrder (
        var oreder: Int? = null,
        var shopId: String? = null,
        var isDeleted: Boolean = false
){
    @Id
    @GeneratedValue
    var id: Long? = null

    @ManyToOne
    var trip : Trip? = null
}